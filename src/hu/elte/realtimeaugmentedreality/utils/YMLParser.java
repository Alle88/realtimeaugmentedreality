package hu.elte.realtimeaugmentedreality.utils;

import hu.elte.realtimeaugmentedreality.MainActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.opencv.core.Size;

import android.os.Environment;
import android.util.Log;

public class YMLParser {	
	private static final String TAG = "RAR::Parser";
	public static final String yamlFileName = "calibData.yml";
	private File yamlFile;
	private File storagePath;	
	
	//XML nodes:
	private static final String ErrReprojBegin = "errReproj:";
	private static final String CameraMatrixBegin = "Camera_Matrix:";
	private static final String DistorsionCoeffBegin = "Distortion_Coefficients:";
	private static final String rowsBegin = "rows:";
	private static final String colsBegin = "cols:";
	private static final String dataBegin = "data:";
	private static final String opencvStorageBegin = "opencv_storage:";
	public static final String imagesBegin = "images_name:";
	private static final String imagesResolutionBegin = "images_resolution:";
	private static final String patternSizeBegin = "pattern_size:";

	private String images, errReproj;
	private String cmRows, cmCols, cmData;	
	private String dcRows, dcCols, dcData;
	private int[] imgResolution;
	
	
	public YMLParser() {
		storagePath = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath());
		yamlFile = new File(storagePath + Constant.DIR_NAME + "/" + yamlFileName);	
	}
	
	public boolean parse() {
		Log.i(TAG, "Parsing from " + yamlFileName + " file.");
		BufferedReader br;
		String line = "";
		try {			
			br = new BufferedReader(new FileReader(yamlFile));
			while ((line = br.readLine()) != null) {
				if (line.contains(opencvStorageBegin)) {					
					while ((line = br.readLine()) != null
							&& !line.contains(CameraMatrixBegin)) {						
						if (line.contains(imagesResolutionBegin)) 
							imgResolution = getResolutionFromString(line);					
					}
				}
				if (line.contains(CameraMatrixBegin)) {
					while ((line = br.readLine()) != null
							&& !line.contains(DistorsionCoeffBegin)) {
						if (line.contains(rowsBegin)) 
							cmRows = line.split(":")[1].trim();
						
						if (line.contains(colsBegin))
							cmCols = line.split(":")[1].trim();
											
						if (line.contains(dataBegin)) 
							cmData = line.split(":")[1].trim();						
					}
				}
				if (line.contains(DistorsionCoeffBegin)) {
					while ((line = br.readLine()) != null) {
						if (line.contains(rowsBegin))
							dcRows = line.split(":")[1].trim();
						
						if (line.contains(colsBegin))
							dcCols = line.split(":")[1].trim();
											
						if (line.contains(dataBegin))
							dcData = line.split(":")[1].trim();						
					}
				}
			}
			return true;
		} catch (IOException e) {				
			Log.w(TAG, yamlFileName + " is missing!");
			writeEmptyDataToYamlFile();						
			e.printStackTrace();
			return false;

		} catch (ArrayIndexOutOfBoundsException aie) {			
			Log.e(TAG, yamlFileName + " parsing error!");			
			return false;
		}
	}
	
	
	private int[] getResolutionFromString(String line) {
		int sizeW = 176, sizeH = 144;
		Pattern numberPattern = Pattern.compile("\\d+");
		Matcher numberMatch = numberPattern.matcher(line);
		if (numberMatch.find()){								
			sizeW = Integer.parseInt(numberMatch.group());
			if (numberMatch.find())
				sizeH = Integer.parseInt(numberMatch.group());
		}
		return new int[]{sizeW, sizeH};
	}

	public void writeDataToYamlFile() {					
		FileOperation.writeToNewTextFile(yamlFileName,
		opencvStorageBegin + "\r\n\t" 
				+ imagesBegin  + " " + images + "\r\n\t" + imagesResolutionBegin + " [" + imgResolution[0] +"x" + imgResolution[1] + "]\r\n\t" 
				+ patternSizeBegin + " [" + MainActivity.PATTERN_SIZE.width + "x" + MainActivity.PATTERN_SIZE.height + "]\r\n" +
		CameraMatrixBegin + "\r\n\t" 
				+ ErrReprojBegin + " " + errReproj + "\r\n\t" + rowsBegin + cmRows + "\r\n\t" + colsBegin + cmCols + "\r\n\t" + dataBegin + cmData + "\r\n" +
		DistorsionCoeffBegin + "\r\n\t" 
				+ rowsBegin + dcRows + "\r\n\t" + colsBegin + dcCols + "\r\n\t" + dataBegin + dcData + "\r\n");			
	}	
	
	public void writeEmptyDataToYamlFile() {	
		
		FileOperation.writeToNewTextFile(yamlFileName,
		opencvStorageBegin + "\r\n\t" 
				+ imagesBegin  + "\r\n\t" 
				+ imagesResolutionBegin + "\r\n\t" 
				+ patternSizeBegin + "\r\n" +
		CameraMatrixBegin + "\r\n\t" 
				+ ErrReprojBegin + "\r\n\t" + rowsBegin + "\r\n\t" + colsBegin + "\r\n\t" + dataBegin + "\r\n" +
		DistorsionCoeffBegin + "\r\n\t" 
				+ rowsBegin + "\r\n\t" + colsBegin +  "\r\n\t" + dataBegin + "\r\n");			
	}		

	public double[] getCmDataToArray() {		
			double array[] = new double[Integer.valueOf(cmRows) * Integer.valueOf(cmCols)];
			for (int i = 0; i < array.length; i++) {
				array[i] = getDoubleValue(cmData.split(", ")[i]);
			}
			return array;	}
	
	public double[] getDcDataToArray() {		
		double array[] = new double[Integer.valueOf(dcRows) * Integer.valueOf(dcCols)];
		System.out.println(array[0] + " " + array[1]);
		for (int i = 0; i < array.length; i++) {			
			array[i] = getDoubleValue(dcData.split(", ")[i]);			
		}	
		return array;
	}
	 
	private double getDoubleValue(String input) {		
		Pattern doublePattern = Pattern.compile("(\\-)?(\\d)+(\\.)?(\\d)*");
		Matcher doubleMatcher = doublePattern.matcher(input);
		if (doubleMatcher.find()) {
			return Double.parseDouble(doubleMatcher.group());
		}
		return 0.0;
	}	

	public String getCmRows() {
		return cmRows;
	}

	public String getCmCols() {
		return cmCols;
	}		

	public String getDcRows() {
		return dcRows;
	}

	public String getDcCols() {
		return dcCols;
	}	
	
	public int[] getImageResolution() {
		return imgResolution;
	}
	
	public void setImages(String images, Size imgRes) {
		this.images = images;
		this.imgResolution = new int[]{(int) imgRes.width, (int) imgRes.height};		
	}	

	public void setCmParams(int cmRows, int cmCols, String cmData, double errReproj ) {
		this.cmRows = Integer.toString(cmRows);
		this.cmCols = Integer.toString(cmCols);
		this.cmData = cmData;
		this.errReproj = Double.toString(errReproj);
	}

	public void setDcParams(int dcRows, int dcCols, String dcData) {
		this.dcRows = Integer.toString(dcRows);
		this.dcCols = Integer.toString(dcCols);
		this.dcData = dcData;
	}	
}
