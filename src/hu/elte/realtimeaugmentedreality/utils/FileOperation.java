package hu.elte.realtimeaugmentedreality.utils;

import hu.elte.realtimeaugmentedreality.MainActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class FileOperation {
	private static final String TAG = "RAR::FileOperation";	

	/** Save image to disk*/
	public static void writeImageToFile(MainActivity mainActivity, Mat rgba) {
		if (rgba != null) {
			File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
					+ Constant.DIR_NAME);
			if (!dir.exists()) {
				dir.mkdir();
				Log.i(TAG, "No such folder");
			}
			String currentDateAndTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss")
					.format(new Date());
			String fileName = dir.getAbsolutePath() + "/picture_" + currentDateAndTime + ".png";

			Imgcodecs.imwrite(fileName, rgba);
			Toast.makeText(mainActivity, fileName + " saved", Toast.LENGTH_SHORT).show();
			Log.i(TAG, fileName + " saved");
			FileOperation.writeToTextFileAfterSpecifiedField(YMLParser.yamlFileName,
					YMLParser.imagesBegin, fileName);
		}
	}
	
	public static List<File> listFilesInWorkingDirectory(String path) {
		File folder = new File(path);
		List<File> filesList = new ArrayList<File>();
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isFile())
				filesList.add(fileEntry);
		}
		return filesList;
	}

	public static void writeToNewTextFile(String fileName, String message) {

		File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Constant.DIR_NAME);
		if (!dir.exists()) {
			dir.mkdir();
		}
		String fn = dir.getAbsolutePath() + "/" + fileName;

		try {
			FileOutputStream out = new FileOutputStream(fn);
			out.write(message.getBytes());
			out.flush();
			out.close();
			Log.i(TAG, fileName + " created.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeToTextFileAfterSpecifiedField(String fileName, String nodePattern,
			String message) {

		BufferedReader reader;
		FileWriter writer;
		List<String> text = new ArrayList<String>();
		try {
			String fn = Environment.getExternalStorageDirectory().getAbsolutePath() + Constant.DIR_NAME + "/" + fileName;

			reader = new BufferedReader(new FileReader(fn));

			String line = "";
			while ((line = reader.readLine()) != null) {
				if (line.contains(nodePattern)) {
					text.add(line + " " + message);
				} else
					text.add(line);

			}
			reader.close();

			writer = new FileWriter(new File(fn));
			for (int i = 0; i < text.size(); i++) {
				writer.write(text.get(i) + "\r\n");
				writer.flush();
			}
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void deleteCalibrationImagesFromDevice(MainActivity mainActivity) {

		File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Constant.DIR_NAME);
		if (dir.exists()) {
			File[] fileList = new File(dir.getAbsolutePath()).listFiles();
			for (File f : fileList) {
				if (f.getName().contains(".png")) {
					if (f.delete()) 
						Log.i(TAG, f.getName() + " was deleted!");
					 else 
						Log.i(TAG, "Delete operation has failed.");					
				}
			}
			Toast.makeText(mainActivity, fileList.length + " calibration images have been deleted!",
					Toast.LENGTH_LONG).show();
			Log.i(TAG, fileList.length + " calibration images have been deleted");

		}
	}
	
	public static void deleteCalibrationFileFromDevice(MainActivity mainActivity) {

		File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Constant.DIR_NAME);
		if (dir.exists()) {
			File[] fileList = new File(dir.getAbsolutePath()).listFiles();
			for (File f : fileList) {
				if (f.getName().contains(".yml") || f.getName().contains(".YML") ) {
					if (f.delete()) {
						Log.i(TAG, f.getName() + " was deleted!");
						Toast.makeText(mainActivity, f.getName() + " was deleted!", Toast.LENGTH_LONG).show();
					} else {
						Log.i(TAG, "Delete operation has failed.");	
						Toast.makeText(mainActivity, "Delete operation has failed.", Toast.LENGTH_LONG).show();
					}
				}
			}
		}
	}
}
