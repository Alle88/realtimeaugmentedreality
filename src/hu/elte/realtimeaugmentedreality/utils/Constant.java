package hu.elte.realtimeaugmentedreality.utils;

import org.opencv.core.Scalar;

public class Constant {
	
	public static final Scalar WHITE = new Scalar( 255 , 255, 255, 255);
	public static final Scalar RED = new Scalar( 255 , 0, 0, 255);
	public static final Scalar GREEN = new Scalar( 0 , 255, 0, 255);
	public static final Scalar BLUE = new Scalar( 0 , 0, 255, 255);
	public static final String DIR_NAME = "/ChessBoardDetection";	

}
