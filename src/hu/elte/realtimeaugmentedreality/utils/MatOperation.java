package hu.elte.realtimeaugmentedreality.utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import android.util.Log;

import Jama.Matrix;

/**
 * There are some basic operations
 * @author Alle
 */
public class MatOperation {
	private static final String TAG = "RAR:Mat";
	
	/**Print out the values of OpenCV Mat */
	public static void LoggerHelper(Mat a, String s ) {
		Log.i(TAG, "\n'" + s + "' mátrix (" + a.rows() + "x" + a.cols() + ")");		
		String toString = "\n";
		for (int i = 0; i < a.rows(); i++) {
			toString += "[";
			for (int j = 0; j < a.cols(); j++) {
				toString += a.get(i, j)[0] + ", ";
			}
			toString += "]\r\n";
		}
		
		Log.i(TAG, toString);		
	}
	
	/**Print out the values of Jama matrix */
	public static void LoggerHelper(Matrix a, String s ) {
		Log.i(TAG, "\n'" + s + "' mátrix: " + a.getRowDimension() + "x" + a.getColumnDimension() + "-es lett:");		
		String toString = "\n";
		for (int i = 0; i < a.getRowDimension(); i++) {
			toString += "[";
			for (int j = 0; j < a.getColumnDimension(); j++) {
				toString += a.get(i, j) + ", ";
			}
			toString += "]\r\n";
		}		
		Log.i(TAG, toString);		
	}	
	
	/**Print out the values of an array */
	public static void LoggerHelper(double[] real, String s) {
		Log.i(TAG, "\n'" + s + "' (" + real.length + " hosszú vektor)");
		for (int i = 0; i < real.length; i++) {
			Log.i(TAG, real[i] + "" );			
		}
	}
	
	public static void LoggerHelper(float[] real, String s) {
		// Log.i(TAG, "\n'" + s + "' (" + real.length + " hosszú vektor)");
		String toString = "";
		for (int i = 0; i < real.length; i++) {
			toString += real[i] + " ";
		}
		Log.i(TAG, "[ " + toString + " ]");
	}
	
	/** Fill a Jama matrix with a specified columns of an eigenmatrix*/
	public static Matrix FillJamaMatrixWithEigenVectors(Matrix eigenVectors, int col) {			 
		Matrix jamaMx = new Matrix(3, 3);	
		double data[] = new double[jamaMx.getRowDimension()*jamaMx.getColumnDimension()];			
		
		for (int i = 0; i < eigenVectors.getRowDimension(); i++) {
			data[i] = eigenVectors.get(i, col);				
		}		
				
		jamaMx = fillJamaMatrix(jamaMx, data);
		return jamaMx;
	}
	
	/** Put the elements of a OpenCV Mat to a Jama matrix*/
	public static Matrix fillJamaMatrix(Matrix mx, Mat data) {
		
		for (int i = 0; i < mx.getRowDimension(); i++) {
			for (int j = 0; j < mx.getColumnDimension(); j++) {				
				mx.set(i, j, data.get(i, j)[0]);
			}			
		}
		return mx;
	}
	
	/** Put the elements of an array into a Jama matrix*/
	public static Matrix fillJamaMatrix(Matrix mx, double[] data) {
		
		for (int i = 0; i < mx.getRowDimension(); i++) {
			for (int j = 0; j < mx.getColumnDimension(); j++) {				
				mx.set(i, j, data[ i * mx.getColumnDimension() + j]);// row-cont to col-cont
			}			
		}
		return mx;
	}
	
	/** Convert a Jama matrix to OpenCV Mat*/
	public static Mat convertJama2OCV(Matrix inp) {
		Mat out = new Mat(inp.getRowDimension(), inp.getColumnDimension(), CvType.CV_64F);
		for (int i = 0; i < inp.getRowDimension(); i++) {
			for (int j = 0; j < inp.getColumnDimension(); j++) {				
				out.put(i, j, inp.get(i, j));
			}			
		}
		return out;
	}	
	
	/** Convert a mxn dimensional matrix to a 1 dimensional column matrix */
	public static Matrix convertColumnMatrix(Matrix p) {
		Matrix vec = new Matrix(p.getRowDimension()*p.getColumnDimension(), 1);		
		
		for (int i = 0; i < p.getRowDimension(); i++) {
			for (int j = 0; j < p.getColumnDimension(); j++) {		
				vec.set(i * p.getColumnDimension() + j, 0, p.get(i, j));			
			}			
		}	
		
		return vec;
	}
	
	/** Get One columnk from a 3x3 dimensional matrix */
	public static Matrix getOneColFromMatrix(Matrix inp, int col) {			 
		Matrix jamaMx = new Matrix(3, 1);	
		double data[] = new double[jamaMx.getRowDimension()*jamaMx.getColumnDimension()];			
		
		for (int i = 0; i < inp.getRowDimension(); i++) {
			data[i] = inp.get(i, col);				
		}			
		jamaMx = fillJamaMatrix(jamaMx, data);
		return jamaMx;
	}
	
	/** Subtract two vectors*/
	public static double[] vectorSub(double[] a, double[] b) {		
		double[] c = new double[a.length];
		for (int i = 0; i < a.length; i++) {
			c[i] = a[i] - b[i];
		}		
		return c;		
	}	
	
	/**Fill the elemnts of a matrix matrix from column arrays*/
	public static Mat convertFromArraysToOneMatrix(Matrix r1, Matrix r2, Matrix r3) {
		Mat out = new Mat(r1.getRowDimension(), 3, CvType.CV_64F);
		
		for (int i = 0; i < r1.getRowDimension(); i++) {			
				out.put(i, 0, r1.get(i, 0));
				out.put(i, 1, r2.get(i, 0));		
				out.put(i, 2, r3.get(i, 0));	
		}
		return out;		
	}		
}
