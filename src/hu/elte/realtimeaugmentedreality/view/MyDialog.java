package hu.elte.realtimeaugmentedreality.view;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

public class MyDialog extends DialogPreference {
	private boolean wasPositiveResult;

	public MyDialog(Context context, AttributeSet attrs) {
        super(context, attrs);        
    }
	
	 @Override
	    protected void onDialogClosed(boolean positiveResult) {
	        super.onDialogClosed(positiveResult);

	        if (callChangeListener(positiveResult)) {
	            setValue(positiveResult);
	        }
	    }
	 	/**Sets the value of this preference, and saves it to the persistent store if required. */
	    public void setValue(boolean value) {
	        wasPositiveResult = value;
	        
	        persistBoolean(value);	        
	        notifyDependencyChange(!value);
	    }
	    
	    /**Gets the value of this preference.*/
	    public boolean getValue() {
	        return wasPositiveResult;
	    }  
	 
}
