package hu.elte.realtimeaugmentedreality.view;

import hu.elte.realtimeaugmentedreality.R;
import hu.elte.realtimeaugmentedreality.R.xml;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class PreferenceSetting extends PreferenceActivity{
	private static final String TAG = "RAR::Pref";
	 @Override
	    protected void onCreate(final Bundle savedInstanceState)
	    {
	        super.onCreate(savedInstanceState);
	        addPreferencesFromResource(R.xml.settings);	        
	    } 
	
   
}
