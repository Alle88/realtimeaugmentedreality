package hu.elte.realtimeaugmentedreality;

import hu.elte.realtimeaugmentedreality.utils.MatOperation;
import hu.elte.realtimeaugmentedreality.utils.YMLParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point3;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import android.util.Log;
import android.widget.Toast;

public class CalibrateCameraManager {		
	
	private static final String TAG = "RAR:CCM";
	private YMLParser ymlParse;
	
	private List objectPoints;
	private List imagePoints;	
	private ArrayList<Mat> imageMatList;
	
	private Mat cameraMatrix = Mat.eye(3, 3, CvType.CV_64F);
	private Mat distCoeffs = Mat.zeros(5, 1, CvType.CV_64F); //lens distortion parameters
	private List<Mat> rvecs = new ArrayList<Mat>(); //Output vector of rotation vectors
	private List<Mat> tvecs = new ArrayList<Mat>(); //Output vector of translation vectors estimated for each pattern view.
	int flagsCalib = Calib3d.CALIB_ZERO_TANGENT_DIST | Calib3d.CALIB_FIX_PRINCIPAL_POINT | Calib3d.CALIB_FIX_K4 | Calib3d.CALIB_FIX_K5;
	
	private MatOfDouble mDistCoeffs = new MatOfDouble();		
	public MatOfPoint2f mCubeVerticesImageSpace = new MatOfPoint2f();
	
	private MatOfPoint3f mCornersInChessSpace3f;
	private MatOfPoint3f mCubeVertices;
	private Mat mRVecs = new Mat();
	private Mat mTVecs = new Mat();
	
	
	public CalibrateCameraManager() {
		super();
		int s = 2;
		//Intial coordinates of the cube:
		int a = (int)((MainActivity.PATTERN_SIZE.width)/2)-1;
		int b = (int)((MainActivity.PATTERN_SIZE.height)/2)-1;		
		
		mCubeVertices = new MatOfPoint3f(new Point3(a, b, 0), //0
										new Point3(a+s, b, 0), //1
										new Point3(a, s+b, 0), //2
										new Point3(a, b, -s), //3
										
										new Point3(a+s, s+b, 0),  //4
										new Point3(a, s+b, -s),  //5
										new Point3(a+s, b, -s),  //6
										new Point3(a+s, s+b, -s)); //7			
		mCornersInChessSpace3f = getCorner3f();		
		ymlParse = new YMLParser();
	}

	public void getAllCornersFromImages(String path) {		
		imageMatList = new ArrayList<Mat>();
		objectPoints = new ArrayList<MatOfPoint3f>();
		imagePoints = new ArrayList<MatOfPoint2f>();		
		String images = "";
		for (File f : new File(path).listFiles()) {			
			if (f.getName().contains(".png")) {				
				images += " " + f.getName();				
				Mat mat = Imgcodecs.imread(f.getPath(), Imgcodecs.CV_LOAD_IMAGE_COLOR);			
				if (mat == null || mat.channels() != 3)
					continue;
				Mat gray = new Mat();
				Imgproc.cvtColor(mat, gray, Imgproc.COLOR_BGR2GRAY);
				MatOfPoint2f corners2f = new MatOfPoint2f();		
				if (!getChessBoardCornersFromOneImage(gray, corners2f, f.getName())) {
					continue;
				}					
				objectPoints.add(mCornersInChessSpace3f);
				imagePoints.add(corners2f);
				imageMatList.add(mat);				
			}
		}
		if (imageMatList.size() != 0)
			ymlParse.setImages(images, imageMatList.get(0).size());
	}
	
	/**generate MatOfPoint3f for the points on the chessboard */
	private MatOfPoint3f getCorner3f() {
		MatOfPoint3f corner3f = new MatOfPoint3f();
		double squareSize = 1;									
		Point3[] vp = new Point3[(int) (MainActivity.PATTERN_SIZE.height * MainActivity.PATTERN_SIZE.width)];
		int cnt = 0;
		for (int i = 0; i < MainActivity.PATTERN_SIZE.height; ++i) {
			for (int j = 0; j < MainActivity.PATTERN_SIZE.width; ++j, cnt++) {
				vp[cnt] = new Point3(j * squareSize, i * squareSize, 0.0d);					
			}			
		}
		corner3f.fromArray(vp);		
		return corner3f;
	}
	
	public void calibrateCameraFromImages(MainActivity mainActivity, boolean ocvCalibratonModeEnabled) {
		if (imageMatList.size() > 1) {
			double errReproj;
			if (ocvCalibratonModeEnabled) {
				Log.i(TAG, "OpenCV implementation of calibration");
				errReproj = Calib3d.calibrateCamera(objectPoints, imagePoints, imageMatList.get(0)
						.size(), cameraMatrix, distCoeffs, rvecs, tvecs, flagsCalib);				
			} else {
				Log.i(TAG, "Own implementation of calibration");
				errReproj = MyCalibrateCamera.calibrateCamera(objectPoints, imagePoints,
						imageMatList, cameraMatrix, distCoeffs, rvecs, tvecs);
				MatOperation.LoggerHelper(cameraMatrix, "kameramátrix");

			}
			ymlParse.setCmParams(cameraMatrix.height(), cameraMatrix.width(),
					MatToString(cameraMatrix.dump()), errReproj);
			ymlParse.setDcParams(distCoeffs.height(), distCoeffs.width(),
					MatToString(distCoeffs.dump()));
			MainActivity.CAMERACALIB_SUCCESS = true;
			ymlParse.writeDataToYamlFile();
		} else {
			Log.e(TAG, "Not enough calibration images!");
			Toast.makeText(mainActivity, "Not enough images. Please take more!", Toast.LENGTH_LONG)
					.show();
		}
	}
	
	private String MatToString(String string) {		
		string = string.replaceAll(";\n", ", ").replaceAll("   ", " ").replaceAll("  ", " ");		
		return string;
	}	
	
	public boolean calibrateCameraFromYamlFile(MainActivity mainActivity) {
		if ( ymlParse.parse() ) {				
			cameraMatrix.put(0, 0, ymlParse.getCmDataToArray());
			mDistCoeffs.put(0, 0, ymlParse.getDcDataToArray());
			changeResolution(ymlParse.getImageResolution(), MainActivity.RESOLUTION);
			Log.i(TAG, "Calibrated from an existing file.");	
			return true;
		} else {
			Log.e(TAG, "Calibrated parameters are missing from file.\n"); 		
			Toast.makeText(mainActivity, "Please calibrate first!", Toast.LENGTH_LONG).show();	
			return false;
		}
	}
	
	/**Object pose estimation from 3D-2D point correspondences.*/	
	public void runPnP(Mat rgba, MatOfPoint2f chessBoardCornersf) {	
		
		//Perspective-n-Point Camera Pose Estimation:
		Calib3d.solvePnP(mCornersInChessSpace3f, chessBoardCornersf, cameraMatrix, mDistCoeffs, mRVecs, mTVecs);

		// Efficient Perspective-n-Point Camera Pose Estimation:		
		// Calib3d.solvePnP(mCornersInChessSpace3f, chessBoardCornersf, cameraMatrix, mDistCoeffs, mRVecs, mTVecs, true, Calib3d.CV_EPNP);

		// Projects 3D points to an image plane:
		Calib3d.projectPoints(mCubeVertices, mRVecs, mTVecs, cameraMatrix, mDistCoeffs,	mCubeVerticesImageSpace);

	}	
	
	private boolean getChessBoardCornersFromOneImage(Mat gray, MatOfPoint2f corners, String fname) {		
		if (!Calib3d.findChessboardCorners(gray, MainActivity.PATTERN_SIZE, corners,
				MainActivity.CALIB_PARAMS)) {
			Log.e("RAR", "Not founded valid corners (" +  MainActivity.PATTERN_SIZE.toString() + ") on image: " + fname);			
			return false;
		}			
		return true;
	}
	
	public void changeResolution(int[] oldRes, int[] newRes){
		double aspectW = (double)newRes[0] / (double)oldRes[0];
		double aspectH = (double)newRes[1] / (double)oldRes[1];
		
		double fku = cameraMatrix.get(0, 0)[0];
		double fkv = cameraMatrix.get(1, 1)[0];
		double u = cameraMatrix.get(0, 2)[0];
		double v = cameraMatrix.get(1, 2)[0];		
		
		cameraMatrix.put(0, 0, fku * aspectW);
		cameraMatrix.put(1, 1, fkv * aspectH);
		cameraMatrix.put(0, 2, u * aspectW);
		cameraMatrix.put(1, 2, v * aspectH);				

		Log.i(TAG, "Change resolution " + oldRes[0] + "x" + oldRes[1] + " --> " + newRes[0] + "x" + newRes[1] 
				+ "  ratio: (" + aspectW + "," + aspectH + ")");	
		
	}

}
