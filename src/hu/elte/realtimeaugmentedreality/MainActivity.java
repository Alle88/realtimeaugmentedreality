package hu.elte.realtimeaugmentedreality;

import java.util.HashSet;
import java.util.Iterator;
import hu.elte.realtimeaugmentedreality.utils.Constant;
import hu.elte.realtimeaugmentedreality.utils.FileOperation;
import hu.elte.realtimeaugmentedreality.view.PreferenceSetting;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

public class MainActivity extends Activity implements CvCameraViewListener2, OnTouchListener {
	private static final String TAG = "RAR::Main";
	
	public static Size PATTERN_SIZE = new Size(6, 7);
	public static int CALIB_PARAMS = 0;			
	public static int[] RESOLUTION = {176, 144};
	public static boolean CAMERACALIB_SUCCESS = false;	
	static TermCriteria CRITERIA = new TermCriteria(TermCriteria.EPS
            + TermCriteria.MAX_ITER, 30, 0.001);
	
	private CameraBridgeViewBase OpenCvCameraView;
	
	private MatOfPoint2f[] chessBoardCorners = new MatOfPoint2f[5]; 
	private int lastValidCorner = -1;
	private boolean foundCorners = false;
	private boolean showChessCornersEnabled = false;
	private boolean ocvCalibratonModeEnabled = false;
	private boolean photoModeEnabled = false;

	private Mat IntermediateMat;
	private Mat rgba;
	private Mat rgba_raw; 
	private CalibrateCameraManager ccm;		

	
	public MainActivity() {
		Log.i(TAG, "Instantiated new " + this.getClass());
	}
	
	static {
	    if (!OpenCVLoader.initDebug()) {
	    	 Log.e(TAG, "OCV JNI init error");
	    }
	}

	/** Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "called onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_main_layout);
		OpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.realtime_ar_view);
		OpenCvCameraView.setCvCameraViewListener(this);
		OpenCvCameraView.setMaxFrameSize(RESOLUTION[0], RESOLUTION[1]);
		resetAllSettingValues();
		reCalibrateCameraFromFile(PATTERN_SIZE);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		Log.i(TAG, "onPause");
		if (OpenCvCameraView != null)
			OpenCvCameraView.disableView();
	}	

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this,
				mLoaderCallback);
		resetSettingValues();
		Log.i(TAG, "onResume: " + RESOLUTION[0] + "x" + RESOLUTION[1] + ", " + PATTERN_SIZE.width+"x"+PATTERN_SIZE.height);
		resetAllSettingValues();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (OpenCvCameraView != null)
			OpenCvCameraView.disableView();
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {			
		
		//Capture screenshot:
		if ( foundCorners && photoModeEnabled)
			FileOperation.writeImageToFile(this, rgba_raw);		
		return false;
	}

	@Override
	/** Initializing Menu XML file (res/menu.xml)*/
	public boolean onCreateOptionsMenu(Menu menu) {
		
		Log.i(TAG, "called onCreateOptionsMenu");		
		getMenuInflater().inflate(R.layout.menu, menu);	
		return true;
	}
	
	@Override
    protected void onStop() {    
     super.onStop();
    } 

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {	
        
		switch(item.getItemId()) {	
		case R.id.syssettings:	
			startActivityForResult(new Intent(this, PreferenceSetting.class), 1);	
	 	 return true;
		}		
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Getting values from Settings menu:
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		boolean deleteImg = sharedPrefs.getBoolean("deleteImagesKey", false);
		boolean deleteFile = sharedPrefs.getBoolean("deleteYamlFileKey", false);
		boolean calibrate = sharedPrefs.getBoolean("calibrateModeKey", false);	
		String resolutionString = sharedPrefs.getString("resolutionKey", RESOLUTION[0] + "x" + RESOLUTION[1]);
		String patternSizeString = sharedPrefs.getString("patternSizeKey", PATTERN_SIZE.width+"x"+PATTERN_SIZE.height);
		HashSet<String> parametersSet = (HashSet<String>) sharedPrefs.getStringSet("calibParamListKey", new HashSet<String>());		
		showChessCornersEnabled = sharedPrefs.getBoolean("showChessBoarCornersKey", false);
		ocvCalibratonModeEnabled = sharedPrefs.getBoolean("calibrationModeKey", false);		
		photoModeEnabled = sharedPrefs.getBoolean("photoModeKey", false);			
		
		
		if (deleteImg) 
			deleteImages();			
			
		if (deleteFile) 
			deleteYamlFile();
			
		//Camera calibration mode:
		if (calibrate) 
			cameraCalibration();		
		
		//Camera calibration flags:
		int newCalibParams =  0;
	    for (Iterator<String> it = parametersSet.iterator(); it.hasNext();) {
	       newCalibParams += Integer.parseInt(it.next());	      
	    }	
	    if (CALIB_PARAMS != newCalibParams)
	    	CALIB_PARAMS = newCalibParams;
	    Log.i(TAG, "Params: " + CALIB_PARAMS);
		
		//Resolution menu:		
		int w = Integer.parseInt(resolutionString.split("x")[0]);
		int h = Integer.parseInt(resolutionString.split("x")[1]);		
		if (RESOLUTION[0] != w && RESOLUTION[1] != h)  {			
			updateCamera(new int[] {w, h});
		}
		
		int x = Integer.parseInt(patternSizeString.split("x")[0]);
		int y = Integer.parseInt(patternSizeString.split("x")[1]);
		if (PATTERN_SIZE.width != x && PATTERN_SIZE.height != y)  {			
			reCalibrateCameraFromFile(new Size(x, y));
		}
		
		resetSettingValues();
	}
	
	private void updateCamera(int[] newResolution) {
		ccm.changeResolution(RESOLUTION, newResolution);
		RESOLUTION = newResolution;
		OpenCvCameraView.disableView();
		OpenCvCameraView.setMaxFrameSize(RESOLUTION[0], RESOLUTION[1]);
		OpenCvCameraView.enableView();
	}

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");
				OpenCvCameraView.enableView();
				OpenCvCameraView.setOnTouchListener(MainActivity.this);
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	public void onCameraViewStarted(int width, int height) {
		IntermediateMat = new Mat();
	}

	public void onCameraViewStopped() {
		// Explicitly deallocate Mats
		if (IntermediateMat != null)
			IntermediateMat.release();
		IntermediateMat = null;
	}
	
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		
		rgba = inputFrame.rgba();	
		rgba_raw = inputFrame.rgba().clone();

		// CHESSBOARD detection:				
		MatOfPoint2f tmpCorners = new MatOfPoint2f();		
		foundCorners = Calib3d.findChessboardCorners(rgba, PATTERN_SIZE, tmpCorners, CALIB_PARAMS);  	
		
		if (foundCorners) {					
			 
			if (lastValidCorner == -1) {
				lastValidCorner = 0;
				chessBoardCorners[lastValidCorner] = tmpCorners;
			} else {
				lastValidCorner = (lastValidCorner+1) % 5;
				chessBoardCorners[lastValidCorner] = tmpCorners;
				
				drawChessBoardCorners();								
				
				if (CAMERACALIB_SUCCESS) {
					ccm.runPnP(rgba_raw, chessBoardCorners[lastValidCorner]);
					drawOpenCVCube();
				}
			}			
		}

		return rgba;
	}	

	/** Draw colored circles where the detected ChessboardCorners are, if this setting item is enabled */
	private void drawChessBoardCorners() {		
		if (showChessCornersEnabled) {
			// Draw chessboardcorners:
			int w = (int) PATTERN_SIZE.width;
			for (int i = 0; i < chessBoardCorners[lastValidCorner].height(); i++) {
				int idx_i = i % w;
				int idx_j = i / w;
				double u = idx_i / PATTERN_SIZE.width;
				double v = idx_j / PATTERN_SIZE.height;
				Imgproc.circle(rgba, new Point(chessBoardCorners[lastValidCorner].get(i, 0)), 1,
						new Scalar(u * 255, (2 - u - v) / 2.0 * 255, v * 255, 255), 2);
			}
		}
	}
	
	private void cameraCalibration() {		
		String pathStorage = Environment.getExternalStorageDirectory().getAbsolutePath() + Constant.DIR_NAME;					
		Toast.makeText(this, "Please wait, this process may take long.", Toast.LENGTH_LONG).show();		
		ccm = new CalibrateCameraManager();
		
		//long startTime = System.nanoTime();		
		ccm.getAllCornersFromImages(pathStorage);		
		ccm.calibrateCameraFromImages(this, ocvCalibratonModeEnabled);
		//Log.i(TAG, "Time: " + (System.nanoTime()- startTime) / 1000000000.0);
	}

	/** Draw wireframe cube */
	private void drawOpenCVCube() {		

		int thicknes = 1;
		
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(0, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(1, 0)),
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(0, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(2, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(0, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(3, 0)), 
						Constant.GREEN, thicknes );		
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(1, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(4, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(1, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(6, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(2, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(5, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(2, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(4, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(3, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(5, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(3, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(6, 0)), 
						Constant.GREEN, thicknes );		
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(3, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(5, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(3, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(6, 0)), 
						Constant.GREEN, thicknes );		
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(7, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(6, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(7, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(5, 0)), 
						Constant.GREEN, thicknes );
		Imgproc.line(rgba, new Point(ccm.mCubeVerticesImageSpace.get(7, 0)), 
						new Point(ccm.mCubeVerticesImageSpace.get(4, 0)), 
						Constant.GREEN, thicknes );			
	}	
	
	private void reCalibrateCameraFromFile(Size size) {		
		PATTERN_SIZE = size;
		
		ccm = new CalibrateCameraManager();
		CAMERACALIB_SUCCESS = ccm.calibrateCameraFromYamlFile(this);		
	}	
	
	private void resetSettingValues() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		sharedPrefs.edit().remove("deleteImagesKey").commit();
		sharedPrefs.edit().remove("deleteYamlFileKey").commit();
		sharedPrefs.edit().remove("calibrateModeKey").commit();		
	}
	
	private void resetAllSettingValues() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		resetSettingValues();
		sharedPrefs.edit().remove("showChessBoarCornersKey").commit();
		sharedPrefs.edit().remove("calibrationModeKey").commit();
		sharedPrefs.edit().remove("resolutionKey").commit();
		sharedPrefs.edit().remove("calibParamListKey").commit();
		sharedPrefs.edit().remove("patternSizeKey").commit();
		sharedPrefs.edit().remove("photoModeKey").commit();
	}	
	
	private void deleteImages() {
		FileOperation.deleteCalibrationImagesFromDevice(this);		
	}
	private void deleteYamlFile() {
		FileOperation.deleteCalibrationFileFromDevice(this);		
	}

}
